var home=document.getElementById("home1");
var carte=document.getElementById("carte1");
var resa=document.getElementById("reserver1");
var avis=document.getElementById("avis1");
var contact=document.getElementById("contact1");
var partenaires=document.getElementById("part1");
var burger=document.getElementsByClassName("burger");
var menuburger=document.getElementsByClassName("menuburger");
var couleur;
var back;
var taille;
var logo=document.getElementById("petitlogo");


home.addEventListener("mouseover",souris);
carte.addEventListener("mouseover",souris);
resa.addEventListener("mouseover",souris);
avis.addEventListener("mouseover",souris);
contact.addEventListener("mouseover",souris);
partenaires.addEventListener("mouseover",souris);
home.addEventListener("mouseout",out);
carte.addEventListener("mouseout",out);
resa.addEventListener("mouseout",out);
logo.addEventListener("mouseout",out);
avis.addEventListener("mouseout",out);
contact.addEventListener("mouseout",out);
partenaires.addEventListener("mouseout",out);
burger[0].addEventListener("click",affichagemenu);
window.addEventListener('resize', redim);
logo.addEventListener("mouseover",souris1);


function affichagemenu(){
    if (menuburger[0].style.display=="none") {
        menuburger[0].style.display="flex";
    } else {
        menuburger[0].style.display="none";
    }
}

function souris() {
    couleur=this.style.color;
    back=this.style.backgroundColor
    this.style.color="#3A2D47";
    this.style.backgroundColor="#ede3df";
}

function out() {
    this.style.color=couleur;
    this.style.backgroundColor=back;
    logo.style.width=taille;
}

function redim() {
    var largeur =  window.innerWidth;
    if (largeur>=1400){
        menuburger[0].style.display="none";
    }
}

function souris1() {
    taille=logo.style.width;
    logo.style.width="150px";
}